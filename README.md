## Tenserflow - ML

### Install
```
pip install -r requirements.txt
```

### Run
#### Clasify clothes
- if you want you can add more images in `./data/clothes` directory.
```
python clasify_clothes.py
```
![Example](.docs/example0.png)
#### Clasify graphs
- if you want you can add more images in `./data/anything/testing` directory.
```
python clasify_anything.py
```
![Example](.docs/example1.png)
