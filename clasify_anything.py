import os
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from tensorflow import keras

from utils import plot_image, get_image, plot_value_array

USE_SAVED_MODEL = True
MODEL_FILENAME = 'anything.h5'
TRAINING_PATH = "./data/anything/training"
TESTING_PATH = "./data/anything/testing"
VALID_IMAGES = [".png", ".jpg", ".jpeg"]


# Get training&testing data
def get_images_from_class_names_directories(root_path):
    return [
        (os.path.join(root_path, class_folder, f), class_folder)
        for class_folder in os.listdir(root_path)
        for f in os.listdir(os.path.join(root_path, class_folder))
        if os.path.splitext(f)[1].lower() in VALID_IMAGES
    ]


train_data = [
    (get_image(img_path), class_name)
    for img_path, class_name in get_images_from_class_names_directories(TRAINING_PATH)
]

train_images = np.array([
    image
    for image, class_name in train_data
])

class_names = list(set([
    class_name
    for image, class_name in train_data
]))

train_labels = np.array([
    class_names.index(class_name)
    for image, class_name in train_data
])

test_data = [
    (get_image(img_path), class_name)
    for img_path, class_name in get_images_from_class_names_directories(TESTING_PATH)
]

test_images = np.array([
    image
    for image, class_name in test_data
])

test_labels = np.array([
    class_names.index(class_name)
    for image, class_name in test_data
])


def get_model():
    if USE_SAVED_MODEL:
        model = keras.models.load_model(MODEL_FILENAME)
        model.compile(
            optimizer=tf.train.AdamOptimizer(),
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy'],
        )
    else:
        model = keras.Sequential([
            keras.layers.Flatten(input_shape=(28, 28)),
            keras.layers.Dense(128, activation=tf.nn.relu),
            keras.layers.Dense(len(class_names), activation=tf.nn.softmax)
        ])
        model.compile(
            optimizer=tf.train.AdamOptimizer(),
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy'],
        )
        model.fit(train_images, train_labels, epochs=20)
        model.save(MODEL_FILENAME)
    return model


model = get_model()


def show_trained_data(images, labels):
    test_loss, test_acc = model.evaluate(images, labels)
    print('Test accuracy:', test_acc)
    predictions = model.predict(images)
    # Plot the first X test images, their predicted label, and the true label
    # Color correct predictions in blue, incorrect predictions in red
    num_rows = len(images)
    num_cols = 1
    num_images = len(images)
    plt.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))
    for i in range(num_images):
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 1)
        plot_image(i, predictions, labels, images, class_names)
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 2)
        plot_value_array(i, predictions, labels)
        _ = plt.xticks(range(len(class_names)), class_names, rotation=45)
    plt.show()


if __name__ == '__main__':
    print('Starting')
    show_trained_data(train_images, train_labels)
    show_trained_data(test_images, test_labels)
