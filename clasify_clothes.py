import os
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from PIL import Image, ImageOps
from tensorflow import keras

from utils import plot_value_array, plot_image

USE_SAVED_MODEL = True

# Initialize model
fashion_mnist = keras.datasets.fashion_mnist

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

train_images = train_images / 255.0
test_images = test_images / 255.0

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

if USE_SAVED_MODEL:
    model = keras.models.load_model('model.h5')
else:
    model = keras.Sequential([
        keras.layers.Flatten(input_shape=(28, 28)),
        keras.layers.Dense(128, activation=tf.nn.relu),
        keras.layers.Dense(10, activation=tf.nn.softmax)
    ])

model.compile(
    optimizer=tf.train.AdamOptimizer(),
    loss='sparse_categorical_crossentropy',
    metrics=['accuracy'],
)


def train():
    model.fit(train_images, train_labels, epochs=15)
    model.save('model.h5')
    test_loss, test_acc = model.evaluate(test_images, test_labels)
    print('Test accuracy:', test_acc)


def show_trained_data():
    predictions = model.predict(test_images)
    # Plot the first X test images, their predicted label, and the true label
    # Color correct predictions in blue, incorrect predictions in red
    num_rows = 5
    num_cols = 3
    num_images = num_rows * num_cols
    plt.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))
    for i in range(num_images):
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 1)
        plot_image(i, predictions, test_labels, test_images, class_names)
        plt.subplot(num_rows, 2 * num_cols, 2 * i + 2)
        plot_value_array(i, predictions, test_labels)
    plt.show()


def test_model(img_file):
    img = Image.open(img_file)
    img = img.convert('L')
    img = img.resize((28, 28))
    img = ImageOps.invert(img)
    test_images = np.array([np.asarray(img, dtype=np.uint8)])
    test_images = test_images / 255.0

    predictions = model.predict(test_images)

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.imshow(test_images[0], cmap=plt.cm.binary)
    plt.grid(False)

    plt.subplot(2, 1, 2)
    plot_value_array(0, predictions, test_labels)
    _ = plt.xticks(range(10), class_names, rotation=45)
    plt.show()


def get_images_paths():
    imgs = []
    path = "./data/clothes"
    valid_images = [".png", ".jpg", ".jpeg"]
    for f in os.listdir(path):
        ext = os.path.splitext(f)[1]
        if ext.lower() not in valid_images:
            continue
        imgs.append(os.path.join(path, f))
    return imgs


if __name__ == '__main__':
    print('Starting')
    if USE_SAVED_MODEL is False:
        train()

    show_trained_data()
    imgs = get_images_paths()
    for img_path in imgs:
        test_model(img_path)
